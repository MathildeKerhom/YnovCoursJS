let splitIdentik = (array) => {
  let newArray = [];
  let checker = {};
  for (let i = 0; i < array.length; i++) {
    let item = array[i];
    if(!checker.hasOwnProperty(item)) {
      checker[item] = newArray.length;
      newArray.push([item]);
    } else {
      newArray[checker[item]].push(item);
    }
  }

  return newArray;
}

let summarizeBasket = (prices, products) => {

  let productNumbers = products.map(item => prices[item]);

  let basketWithDiscount = discount(splitIdentik(productNumbers));

  let arrayPrices = basketWithDiscount.map(subarray => sum(subarray));

  let price = sum(arrayPrices);

  return {
    price: price,
    countArticles: products.length,
    countProducts: getUnique(products).length
  }
}

let sum = (numbers) => {
	return numbers.reduce((acc, number) => {
		return acc + number;
	}, 0);
};

let getUnique = (array) => {
	let newArray = [];
	let checker = {};
	for (let i = 0; i < array.length; i++) {
		let item = array[i];
		if(!checker[item]) {
			checker[item] = true;
			newArray.push(item);
		}
	}

	return newArray;
};

let getCorrectPrice = (array) => {
	var foundProducts = {};
	array.forEach(product => {
		if(!foundProducts[product]) {
			foundProducts[product] = 0;
		}

		foundProducts[product]++;
	});

	return foundProducts;
};

let discount = (array) => {
	// array.slice(0) permet de retourner une nouvelle reference du tableau et d'eviter les effets de bord
	let newArray = array.slice(0);

	return newArray.map(subarray => {
		return subarray.filter((item, index) => (index+1)%3 !== 0);
	});
};

module.exports = {
	splitIdentik: splitIdentik, 
	summarizeBasket: summarizeBasket,
	getUnique: getUnique,
	discount: discount
};