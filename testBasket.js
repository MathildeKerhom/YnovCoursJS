var expect = require('chai').expect;
var summarizeBasket = require('./basket.js').summarizeBasket;
var getUnique = require('./basket.js').getUnique;
var splitIdentik = require ('./basket.js').splitIdentik;
var discount = require('./basket.js').discount;

let prices = {banana: 1, potato: 2, tomato: 3, cucumber: 4, salad: 5, apple: 6};
let products = ['tomato', 'cucumber', 'tomato', 'salad', 'potato', 'cucumber', 'potato', 'potato', 'tomato', 'potato'];

describe('summarizeBasket', function() {
	it('Empty basket should return 0 for each value', function() {
		var result = summarizeBasket({}, []);
		expect(result.price).to.be.equal(0);
		expect(result.countArticles).to.be.equal(0);
		expect(result.countProducts).to.be.equal(0);
	})

	it('Basket containing a single article (tomato) should return the price of that article (3) and 1 for other values', function() {
		var result = summarizeBasket(prices, ['tomato']);
		expect(result.price).to.be.equal(3);
		expect(result.countArticles).to.be.equal(1);
		expect(result.countProducts).to.be.equal(1);
	})

	it('Basket with 2 different products should return basic sum of prices', function() {
		var result = summarizeBasket(prices, ['potato', 'apple']);
		expect(result.price).to.be.equal(8);
		expect(result.countArticles).to.be.equal(2);
		expect(result.countProducts).to.be.equal(2);
	})

	it('Basket with the same 2 products should return basic sum of prices', function() {
		var result = summarizeBasket(prices, ['potato', 'potato']);
		expect(result.price).to.be.equal(4);
		expect(result.countArticles).to.be.equal(2);
		expect(result.countProducts).to.be.equal(1);
	})
});

describe('getUnique() : function to return array with distinct values', function() {
	it('Empty array should return empty array', function() {
		var result = getUnique([]);
		expect(result).to.be.empty;
	})

	it('Array with one value should return array with the same value', function() {
		var result = getUnique(['potato']);
		expect(result).to.be.eql(['potato']);
	})

	it('Array with 2 different values should return array with the same values', function() {
		var result = getUnique(['potato', 'apple']);
		expect(result).to.be.eql(['potato', 'apple']);
	})

	it('Array with 3 values (2 same values, 1 different should return array with 2 values', function() {
		var result = getUnique(['potato', 'apple', 'potato']);
		expect(result).to.be.eql(['potato', 'apple']);
	})
});

describe('splitIdentik() : function to return array with arrays of same values', function() {

  it('Empty array should return empty array', function() {
    var result = splitIdentik([]);
    expect(result).to.be.empty;
  });

  it('Array with one value should return array with same value', function() {
    var result = splitIdentik(['tutu']);
    expect(result).to.be.eql([['tutu']]);
  });

  it('Array with 2 differents values should return array with 2 arrays inside', function() {
    var result = splitIdentik(['titi', 'toto']);
    expect(result).to.be.eql([['titi'], ['toto']]);
  });

  it('Array with 3 items with 2 same values should return array with 2 arrays inside', function() {
    var result = splitIdentik(['tata', 'tete', 'tata']);
    expect(result).to.be.eql([['tata', 'tata'], ['tete']]);
  });

});

describe('discount() : function to return array with discount', function() {
	it('Empty array should return empty array', function() {
	    var result = discount([]);
	    expect(result).to.be.empty;
  	});

  	it('Array with one subarray with one element should return array with one subarray with one element', function() {
	    var result = discount([['tutu']]);
	    expect(result).to.be.eql([['tutu']]);
	});

	it('Array with one subarray with 3 elements should return array with one subarray with 2 elements', function() {
		var result = discount([['tutu', 'tutu', 'tutu']]);
		expect(result).to.be.eql([['tutu', 'tutu']]);
	});
});